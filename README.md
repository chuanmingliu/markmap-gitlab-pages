---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  # initialExpandLevel: 1
  # extraJs: 
  # extraCss: 
  zoom: true   # true by default
  pan: true    # true by default
---

# README

## ℹ️  Make it your own! 

  1. [Fork](https://gitlab.com/brie/markmap-gitlab-pages/-/forks/new) this project. 
  1. Update `README.md`.
  1. Let the pipeline finish.
  1. Enjoy your site!

You can adjust line 7 of `.gitlab-ci.yml` if you want to use a filename other than `README.md` for your mindmap.  

## 🏫  How does it work?

### The mindmap is in regular Markdown

You can write freely -- or bring an existing Markdown document. If you bring existing Markdown, you might need to tweak it to make nodes out of select items.

### `markmap-cli` is used to generate interactive HTML

  - (Try clicking the dots.)

The CI job that runs uses `markmap-cli` to [convert the Markdown](https://www.npmjs.com/package/markmap-cli) to HTML. 

## ✨ [Customize your mindmap!](https://markmap.js.org/docs/json-options)
    
  - Custom Colors
    - HEX welcome 👋
  - Customizable behavior!
  - Custom CSS == welcome!
  - Custom JS? ✅ 

The colors that you specify are processed in order from the top downward.

## 📖 Example

### See [the Markdown file](https://gitlab.com/brie/markmap-gitlab-pages/-/blob/main/README.md) that made this very mindmap.

I have used (or commented) every documented option in this mindmap.

### See the HTML for this mindmap at [markmap.brie.dev](https://markmap.brie.dev).

## 🚀 Next Steps

  - 🛝 [markmap Web playground](https://markmap.js.org/repl)
  - 🏠 [markmap home page](https://markmap.js.org/)

  - 💻 [VSCode Extension](https://marketplace.visualstudio.com/items?itemName=gera2ld.markmap-vscode)  
    - Highly recommended!
  - 📚 [Read the docs](https://markmap.js.org/docs/markmap)
    - Also important!
  - [plugin for LogSeq](https://github.com/vipzhicheng/logseq-plugin-mark-map)